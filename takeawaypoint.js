/* 
DAY--1:
A react variable can store html tag.

we can implement javaScript operation inside tag using curly braces.

there must be only one wrapper in return. i.e only one div.

anything that is written inside return will display in the browser.

Block element: it takes all the width. it always starts from new line. e.g. <div>, <p>

Inline element: it takes only the required space. e.g. <span>.

Image- always place images or file at public folder, . at source of image refers to public folder. 

CSS: 
Inline: 
object is used for styling purpose.
you must use camel case convention.

External: 
it is a three step process. Define,import and use. 

In react console will appear at browser developer panel. Goto there by control+shift+i

DAY--2:
Component: it is a function whose first letter is capital. 
We call component as like calling a html tag. e.g. <App></App>
to obtain boiler-template for controller, use rafce + tab.

passing value on component is props. 

If props is other than string, wrap it with curly braces.

Components are the custom tags.

in-built props/attributes are supported by inbuilt tag and aren't supported by custom tag (components).

DAY--3:
Jsx is an extension, it is a combination of javaScript and HTML. for e.g. in component we create js function and return <div>

Limitations of curly braces:
{} is use to call variable however it has limitations, one is that we can't call two values as it only return one value or doesn't returns value.
it doesn't support if-else statements, for, while loops and we can't declare variables.

Limitations of Ternary operator:
in JavaScript we can write only if statement however in ternary operator, else is mandatory. i.e if and else should be written always.

Effect of different data inside html tag:
Boolean data isn't shown in browser. to show we have to add some logic.

Don't call object inside html tag children

DAY--4:

HandleImg (e)=>{}  if you don'y need to pas the value
HandleImg ()=>{ return ((e)=>{})} if you need to pass the value.

DAY--5:
A page will render if state variable is changed.

When state variable changes
A component gets render such that
the state variable which is changed holds changed values
whereas other state variable holds previous value.


UseEffectFunction: it is asynchronous function. 
It only executes on first render and doesn't get executed from second render. meaning it renders only at beginning. 

useEffect function will run for 1sr render but from second render the execution of function will depend on dependency.

DAY--7:
Lifecycle of Component:
Component did mount(First render)
--useEffect function will run

Component did update (second render)
--useEffect fun will run only if its dependency will change

Component did unmount (component removed)
--nothing gets executed.
--But clean up function will execute

DAY--8:
before working on a react dom--you have to wrap app component by BrowserRouter

DAY--9;
In the input whatever you place at value, it will be displayed in the browser. [input]

DAY--10:
For all input types:      value       e.target.value
For checkbox:             checked     e.target.checked
For Radio-button:          checked     e.target.value          

*/

//local storage is a browser's memory for a particular URL.
//The data in a local storage persist even when the session ends (browser close) (tab close).

//we should always call api within button or inside useEffect function

//always use setState and navigate on button click or inside useEffect(never call directly)
