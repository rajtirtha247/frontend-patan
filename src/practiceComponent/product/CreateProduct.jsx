import axios from 'axios'
import React, { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
  

const CreateProduct = () => {
  let [name, setName]=useState("")
  let [price, setPrice]=useState("")
  let [quantity, setQuantity]=useState("")
  

  let onSubmit=async(e)=>{

    e.preventDefault()
    console.log("the form is submitted")

    let data={
        name:name,
        price:price,
        quantity:quantity
    }
  

    


try {
  setName("")
  setPrice("")
  setQuantity("")

  let result= await axios(
    {
      url:`http://localhost:8000/products`,
      method:`POST`,
      data:data
  
    }
  )
  toast.success(result.data.message, {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
    });

} catch (error) {
  toast.error('Unable to create Product', {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
    });
}

}
  return (

    <div>
       <ToastContainer />
        <form onSubmit={onSubmit}>
            <div><label htmlFor="name"> Name: </label>
            <input type="text" placeholder="e.g. laptop" id='name'
            value={name}
            onChange={(e)=>{
                // console.log("change")
                setName(e.target.value)

            }} ></input>
            </div>

            <div><label htmlFor="price"> Price: </label>
            <input type="number" placeholder="e.g Rs 400" id='price'
            value={price}
            onChange={(e)=>{
                // console.log("change")
                setPrice(e.target.value)

            }} ></input>
            </div>

            
            <div><label htmlFor="quantity"> Quantity: </label>
            <input type="number" placeholder="e.g 2 kg" id='quantity'
            value={quantity}
            onChange={(e)=>{
                // console.log("change")
                setQuantity(e.target.value)

            }} ></input>
            </div>

        
           




            <button type="submit">Proceed</button>
        </form>


    </div>



  )
}

export default CreateProduct