import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const ReadAllProducts = () => {

  let [products, setProduct]=useState([])
  let navigate= useNavigate()

  let getAllProducts=async()=>{
    let result= await axios({
      url:"http://localhost:8000/products",
      method:"GET"
    })

    setProduct(result.data.result)
  }

  let deleteProduct = (product) => {
    return (() => {
     let deleteProduct = async() => {
       try {
         let result = await axios({
           url: `http://localhost:8000/products/${product._id}`,
           method: `delete`
         });
         getAllProducts();
       } catch (error) {
        //  console.log(error.message);
       }
     };
     deleteProduct();
 });
}


  useEffect(()=>{
    getAllProducts()
    

  },[])

  let productInfo= products.map((item, i)=>{
    return (<div key={i} style={{border:"solid blue 2px", margin:"20px", padding:"20px"}}>

      The product name is {item.name}. <br></br>
      The product price is {item.price}. <br></br>
      The product quantity is {item.quantity}.<br></br>

      <button style={{margin:"5px"}} onClick={(e)=>{

        navigate(`/products/${item._id}`)

      }}>View </button>

      <button style={{margin:"5px"}} onClick={(e)=>{ navigate(`/products/update/${item._id}`)
      }} >Edit</button>

      <button style={{margin:"5px"}} onClick={deleteProduct(item)}>Delete</button>


    </div>)
  })
 
  return (
    <div>

      {productInfo}

    </div>
  )
}

export default ReadAllProducts