import React, { useRef } from 'react'

const Learn1UseRefHook = () => {
let ref1=useRef()
let ref2=useRef()

let ref3=useRef()
let ref4=useRef()


  return (
    <div>
        <div onClick={()=>{
            ref3.current.focus()
        }}>  
        Foucs Input 1
        </div>
        <div onClick={()=>{
            ref3.current.blur()
        }}>  
            

            Blur Input 1


        </div>
<div ref={ref1}>Nani</div>
<div ref={ref2}>Babu</div>
<button onClick={()=>{
ref1.current.style.backgroundColor="red"

}}>
    Change Bg color of Babu
</button>

<button onClick={()=>{
ref2.current.style.backgroundColor="aqua"
}}>
    Change Bg color of Nani
</button>

<br></br>
<input ref={ref3}></input> <br></br>
<input ref={ref4}></input>
    </div>
    
  )
}

export default Learn1UseRefHook