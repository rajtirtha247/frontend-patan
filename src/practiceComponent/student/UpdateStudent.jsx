import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const UpdateStudent = () => {
  let params = useParams();

  let [name, setName]=useState("")
  let [age, setAge]=useState("")
  let [isMarried, setIsMarried]=useState(false)

  //calling api to pre-show data on first load
  let getStudent = async () => {
    let result = await axios({
      url: `http://localhost:8000/students/${params.id}`,
      method: "GET",
    });

    // console.log(result)
    let data= result.data.result
    setName(data.name)
    setAge(data.age)
    setIsMarried(data.isMarried)
  };

  useEffect(() => {
    getStudent()
  }, []);

  

  let onSubmit=async(e)=>{

    e.preventDefault()
    console.log("the form is submitted")

    let data={
        name:name,
        age:age,
        isMarried:isMarried
    }  


try {
  setName("")
  setAge("")
  setIsMarried(false)

  let result= await axios(
    {
      url:`http://localhost:8000/students/${params.id}`,
      method:`POST`,
      data:data
  
    }
  )
  toast.success(result.data.message, {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
    });

} catch (error) {
  toast.error('Unable to create Students', {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
    });
}

}


  return (
    <div>

<ToastContainer />
        <form onSubmit={onSubmit}>
            <div><label htmlFor="name"> Name: </label>
            <input type="text" placeholder="e.g. laptop" id='name'
            value={name}
            onChange={(e)=>{
                // console.log("change")
                setName(e.target.value)

            }} ></input>
            </div>

            <div><label htmlFor="age"> Age: </label>
            <input type="number" placeholder="e.g 22" id='age'
            value={age}
            onChange={(e)=>{
                // console.log("change")
                setAge(e.target.value)

            }} ></input>
            </div>

            
            <div><label htmlFor="isMarried"> isMarried: </label>
            <input type="checkbox"  id='isMarried'
            // value={isMarried}
            checked={isMarried===true}
            onChange={(e)=>{
                // console.log("change")
                setIsMarried(e.target.checked)

            }}></input>
            </div>



            <button type="submit">Proceed</button>
        </form>








    </div>
  )
}

export default UpdateStudent