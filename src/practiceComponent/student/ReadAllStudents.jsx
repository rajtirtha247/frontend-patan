import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const ReadAllStudents = () => {
  let [students, setStudent]=useState([])
  let navigate= useNavigate()

  let getAllStudents=async()=>{
    let result= await axios({
      url:"http://localhost:8000/students",
      method:"GET"
    })

    setStudent(result.data.result)
  }

  let deleteStudent = (student) => {
    return (() => {
     let deleteStudent = async() => {
       try {
         let result = await axios({
           url: `http://localhost:8000/students/${student._id}`,
           method: `delete`
         });
         getAllStudents()
        } catch (error) {
        //  console.log(error.message);
       }
     };
     deleteStudent();
 });
}


  useEffect(()=>{
    getAllStudents()

  },[])

  let studentInfo= students.map((item, i)=>{
    return (<div key={i} style={{border:"solid blue 2px", margin:"20px", padding:"20px"}}>

      The student name is {item.name}. <br></br>
      The student age is {item.age}. <br></br>
      The student Marriage status is {item.isMarried? "true": "false"}.<br></br>

      <button style={{margin:"5px"}} onClick={()=>{

        navigate(`/students/${item._id}`)

      }}>View </button>
      <button style={{margin:"5px"}} onClick={(e)=>{ navigate(`/students/update/${item._id}`)
      }}>Edit</button>
      <button style={{margin:"5px"}} onClick={deleteStudent(item)} >Delete</button>


    </div>)
  })
  return (
    <div>

      {studentInfo}
    </div>
  )
}

export default ReadAllStudents