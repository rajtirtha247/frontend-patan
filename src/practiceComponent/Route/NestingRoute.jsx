import React from 'react'
import { Outlet, Route, Routes } from 'react-router-dom'

const NestingRoute = () => {
  return (
    <div>
        <Routes>
            <Route path="/" element={<div><Outlet></Outlet></div>}>
                <Route index element={<div>This is Home Page</div>}></Route>

                <Route path="student" element={<div><Outlet></Outlet></div>}>
                <Route index element={<div> This is Student Page</div>}></Route>

                    <Route path="1" element={<div> This is 1 Page</div>}></Route>
                    <Route path="kamal" element={<div> This is kamal Page</div>}></Route>
                </Route>
                <Route path="*" element={<div>Error 404 Page Not found</div>}></Route>
            </Route>
        </Routes>

    </div>
  )
}

export default NestingRoute
