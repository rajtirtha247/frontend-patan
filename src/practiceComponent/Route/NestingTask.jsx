import React from 'react'
import { Outlet, Route, Routes } from 'react-router-dom'
import CreateProduct from '../product/CreateProduct'
import ReadSpecificProduct from '../product/ReadSpecificProduct'
import ReadAllProducts from '../product/ReadAllProducts'
import UpdateProduct from '../product/UpdateProduct'
import MyLinks from '../MyLinks'
import ReadAllStudents from '../student/ReadAllStudents'
import CreateStudent from '../student/CreateStudent'
import ReadSpecificStudent from '../student/ReadSpecificStudent'
import UpdateStudent from '../student/UpdateStudent'
import AdminRegister from '../Register/AdminRegister'
import AdminVerify from '../Register/AdminVerify'
import AdminLogin from '../Register/AdminLogin'
import AdminProfile from '../Register/AdminProfile'
import AdminLogOut from '../Register/AdminLogOut'
import AdminProfileUpdate from '../Register/AdminProfileUpdate'
import AdminPasswordUpdate from '../Register/AdminPasswordUpdate'
import AdminForgotPassword from '../Register/AdminForgotPassword'
import AdminResetPassword from '../Register/AdminResetPassword'
import AdminReadAllUser from '../Register/AdminReadAllUser'
import AdminReadSpecificUser from '../Register/AdminReadSpecifcUser'
import AdminUpdateSpecificUser from '../Register/AdminUpdateSpecificUser'

const ProductNesting = () => {
  return (
    <div>
        <Routes>
            <Route path="/" element={<div>
             <MyLinks></MyLinks> <Outlet></Outlet>
             {/* <div>This is Footer</div> */}
             </div>}>
             <Route path="reset-password" element={<AdminResetPassword></AdminResetPassword>}></Route>
            <Route index element={<div>This is Home Page</div>}></Route>
            <Route path="verify-email" element={<AdminVerify></AdminVerify>}></Route>
           

            <Route path="products" element={<div><Outlet></Outlet></div>}>
            <Route index element={<ReadAllProducts></ReadAllProducts>}></Route>
            <Route path=":id" element={<ReadSpecificProduct></ReadSpecificProduct>}></Route>
            <Route path="create" element={<CreateProduct></CreateProduct>}></Route>
            <Route path="update" element={<div><Outlet></Outlet></div>}>
            <Route path=":id" element={<UpdateProduct></UpdateProduct>}></Route>
            </Route>
            </Route>

            <Route
            path="students"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllStudents></ReadAllStudents>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificStudent></ReadSpecificStudent>}
            ></Route>
            <Route
              path="create"
              element={<CreateStudent></CreateStudent>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateStudent></UpdateStudent>}
              ></Route>
            </Route>
          </Route>
          
          <Route path="admin" element={<div><Outlet></Outlet></div>}>
                <Route index element={<div>This is admin dash board</div>}></Route>
                <Route path ="register" element={<AdminRegister></AdminRegister>}></Route>
                <Route path="login" element={<AdminLogin></AdminLogin>}></Route>
                <Route path="my-profile" element={<AdminProfile></AdminProfile>}></Route>
                <Route path="logout" element={<AdminLogOut></AdminLogOut>}></Route>
                <Route path="profile-update" element={<AdminProfileUpdate></AdminProfileUpdate>}></Route>
                <Route path="update-password" element={<AdminPasswordUpdate></AdminPasswordUpdate>}></Route>
                <Route path="forgot-password" element={<AdminForgotPassword></AdminForgotPassword>}></Route>
                <Route path="read-all-user" element={<AdminReadAllUser></AdminReadAllUser>}></Route>
                <Route path=":id" element={<AdminReadSpecificUser></AdminReadSpecificUser>}></Route>
                
                <Route path="update" element={<div><Outlet></Outlet></div>}>
                <Route path=":id" element={<AdminUpdateSpecificUser></AdminUpdateSpecificUser>}></Route>
                </Route>
              </Route>

            </Route>
        </Routes>

    </div>
  )
}

export default ProductNesting
