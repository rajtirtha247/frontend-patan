import React from 'react'
import { NavLink } from 'react-router-dom'

const MyLinks = () => {
  return (
    <div>


        <NavLink to="/products/create" style={{marginRight: "20px"}}>Create Products</NavLink>

        <NavLink to="/products" style={{marginRight: "20px"}}> Products</NavLink>

        <NavLink to="/students/create" style={{marginRight: "20px"}}>Create Students</NavLink>

        <NavLink to="/students" style={{marginRight: "20px"}}> Students</NavLink>

        <NavLink to="/admin/register" style={{marginRight: "20px"}}>Admin Register</NavLink>

        <NavLink to="/admin/login" style={{marginRight: "20px"}}>Admin Login</NavLink>

        <NavLink to="/admin/my-profile" style={{marginRight: "20px"}}>Admin Profile</NavLink>
        <NavLink to="/admin/logout" style={{marginRight: "20px"}}>Log out</NavLink>
        <NavLink to="/admin/update-password" style={{marginRight: "20px"}}>Update Password</NavLink>
        <NavLink to="/admin/read-all-user" style={{marginRight: "20px"}}>Read All Users </NavLink>
        

        
        




    </div>
  )
}

export default MyLinks