import React, { useState } from 'react'

const IncrementByChoice = () => {
    let [count, setState]=useState(0)

    let handleIncrement=(increment)=>{

        return (e)=>{
           if(increment>0)
            setState(count+increment)
        else{
            setState(0)
        }
         
        }

    

    }

  return (
    <div>
The count is {count} <br></br>
<button onClick ={handleIncrement(2)}>Increment by 2</button>  
      <button onClick={handleIncrement(100)}>Increment by 100</button>
      <button onClick={handleIncrement(1000)}>Increment by 1000</button>
      <button onClick={handleIncrement(0)}>Reset</button>

        
    </div>
  )
}

export default IncrementByChoice