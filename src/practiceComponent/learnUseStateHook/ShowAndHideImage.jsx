import React, { useState } from 'react'

const ShowAndHideImage = () => {

    let [isVisible, setIsHide]=useState(false)

    let ShowImgFun=()=>{

        setIsHide(true)

    }

    let HideImgFun=()=>{

        setIsHide(false)

    }
  return (
    <div> 
        {
            isVisible?  <img src="./logo192.png" alt="image"></img>:null}
        <button onClick={ShowImgFun}>Show Image</button><br></br>
        <button onClick={HideImgFun}>Hide Image</button>
      
    </div>
  )
}

export default ShowAndHideImage