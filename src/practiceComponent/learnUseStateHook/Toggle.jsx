import React, { useState } from 'react'

const Toggle = () => {
    let [isVisible, setIsHide]=useState(false)

     let handleImg=(e)=>{

          setIsHide(!isVisible) 
        }

    //method--2
    /* 
    
    let handleImg=(e)=>{
        if(isVisible===true){
            setIsHide(false)
        } else{
            setIsHide(true)
        }
    }
    
    */

    
  return (
    <div> 
    {
        isVisible?  <img src="./logo192.png" alt="logo"></img>:null}
    <button onClick={handleImg}>{
    isVisible===true?"Hide":"Show"}</button></div>
  )
}



export default Toggle