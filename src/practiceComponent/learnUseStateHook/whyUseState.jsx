import React, { useState } from 'react'

const WhyUseState = () => {

let [name, setName]=useState("anup")
    // let name="anup"

    console.log("*******")
  return (
    <div>{name}
    
    <button onClick={()=>{
        setName("tirtha")
        
        // name="tirtha"
    }}>Change Name</button>
    </div>
  )
}

export default WhyUseState

/* 

here when setName is called, it re-renders the whole page from the begnining code.

like in first rendering 
first name is anup
then prints *******
then we click change name button, setName gets executed. then second rendering begins. 

in second rendering:
name is tirtha
then prints *******
and Change Name is displayed.

*/