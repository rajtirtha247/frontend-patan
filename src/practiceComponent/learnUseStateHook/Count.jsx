import React, { useState } from 'react'

const Count = () => {

    let [count, setState]=useState(0)
    let [count2, setState2]=useState(100)

    let increment=(e)=>{
        //count++
  
        setState(count+1)
    
    }

    let increment2=(e)=>{
        setState2(count2+1)
    }
    return (
    <div>
count1 is {count} <br></br> <br></br>
count2 is {count2}<br></br>
<br></br>
        <button onClick={increment}>Increment count 1</button>
        <button onClick={increment2}>Increment count 2</button>

    </div>
  )
}

export default Count