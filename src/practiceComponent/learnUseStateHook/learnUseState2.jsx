import React, { useState } from 'react'

const LearnUseStateHook2 = () => {

    let [count, setState]=useState(0)

    let increment=(e)=>{
        //count++
    
       if(count<10){
        setState(count+1)
       }

       else{
        return (0)
       }
      
    }

    let decrement=(e)=>{
        if(count>0){
            setState(count-1)
           }
    
           else{
            return (0)
           }    }

    let reset=(e)=>{
        setState(0)
    }


  return (
    <div>The count is {count}. <br></br>
    <button onClick={increment}>Increment</button>  
      <button onClick={decrement}>Decrement</button>
      <button onClick={reset}>Reset</button>

    
    </div>
  )
}

export default LearnUseStateHook2