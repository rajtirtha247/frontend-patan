import React, { useState } from 'react'

const LearnUseStateHook1 = () => {
    //old way to declare variable== let name="anup"

    //new way to declare vairable
    let [name, setName]= useState("Anup")

    let [age, setAge]= useState(22)

    let handleClick=(e)=>{
        setName("Tirtha")
    }

    let handleAge=(e)=>{
        setAge(23)
    }
  return (
    <div>My Name is {name}. <br></br>
    <button onClick={handleClick}> Change Name</button> <br></br>
    My age is {age}. <br></br>
    <button onClick={handleAge}>Change Age</button>
    </div>
  )
}

export default LearnUseStateHook1

