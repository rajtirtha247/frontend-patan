import axios from 'axios'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';


const AdminLogin = () => {
    
    let [email, setEmail]=useState("")
    let [password, setPassword]=useState("")

    let navigate=useNavigate()
    

    
    
    let onSubmit=async (e)=>{

        e.preventDefault()
        console.log("the form is submitted")

        let data={
            
            email:email,
            password:password,
           
        }

        try {
            let result= await axios({
                url: `http://localhost:8000/weeb-users/login`,
                method: `POST`,
                data: data
            })

            let token=result.data.token

            localStorage.setItem("token", token)

            navigate("/admin")




            
            setEmail("")
            setPassword("")
           
          

        } catch (error) {
            toast.error(error.response.data.message)
        }
    }

    
  

  return (
    <div>
        <ToastContainer></ToastContainer>
  <form onSubmit={onSubmit}>
            

            <div><label htmlFor="email"> Email: </label>
            <input type="email" placeholder="e.g someone@gmail.com" id='email'
            value={email}
            onChange={(e)=>{
                // console.log("change")
                setEmail(e.target.value)

            }} ></input>
            </div>
            

            <div><label htmlFor="password"> Password: </label>
            <input type="password" placeholder="e.g some@123" id='password'
            value={password}
            onChange={(e)=>{
                // console.log("change")
                setPassword(e.target.value)

            }} ></input></div>
        


            <button  type="submit">Login</button>

            <div  onClick={()=>{
                navigate("/admin/forgot-password")
            }}>Forgot Password</div>
        </form>


    </div>
  )
}

export default AdminLogin