import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

const AdminLogOut = () => {
    let navigate=useNavigate()
    localStorage.removeItem("token")

    useEffect(()=>{
        navigate("/")

    },[])
  return (
    <div>AdminLogOut</div>
  )
}

export default AdminLogOut