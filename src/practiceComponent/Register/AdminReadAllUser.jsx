import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const AdminReadAllUser = () => {

  let [users, setUser]=useState([])
  let navigate= useNavigate()

  let getAllUsers=async()=>{
    let result= await axios({
      url:"http://localhost:8000/weeb-users",
      method:"GET",
      headers:{
        Authorization:`Bearer ${localStorage.getItem("token")}`
      }
    })

    setUser(result.data.data)
  }
  let deleteUser = (user) => {
    return (() => {
     let deleteUser = async() => {
       try {
         let result = await axios({
           url: `http://localhost:8000/weeb-users/${user._id}`,
           method: `delete`,
           headers:{
            Authorization:`Bearer ${localStorage.getItem("token")}`
          }

         });
         getAllUsers();
       } catch (error) {
        //  console.log(error.message);
       }
     };
     deleteUser();
 });
}

  useEffect(()=>{
    getAllUsers()
  },[])

  let productInfo= users.map((item, i)=>{
    return (<div key={i} style={{border:"solid blue 2px", margin:"20px", padding:"20px"}}>

      The User Full name is {item.fullName}. <br></br>
      The User email is {item.email}. <br></br>
      The User gender is {item.gender}.<br></br>

      <button style={{margin:"5px"}} onClick={(e)=>{

        navigate(`/admin/${item._id}`)

      }}>View </button>

      <button style={{margin:"5px"}} onClick={(e)=>{ navigate(`/admin/update/${item._id}`)
      }} >Edit</button>
      
      <button style={{margin:"5px"}} onClick={deleteUser(item)}>Delete</button>



    </div>)
  })
 
  return (
    <div>

      {productInfo}

    </div>
  )
}

export default AdminReadAllUser