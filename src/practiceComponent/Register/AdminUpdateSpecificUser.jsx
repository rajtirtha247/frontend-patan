import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';


const AdminUpdateSpecificUser = () => {
    let token=localStorage.getItem("token")

    let navigate= useNavigate()
    let [fullName, setfullName]=useState("")
    
    let [dob, setDob]=useState("")
    let [gender, setGender]=useState("male")

    let params=useParams()

    let id=params.id

    
    
    let onSubmit=async (e)=>{

        e.preventDefault()
        console.log("the form is submitted")

        let data={
            fullName:fullName,
            dob:dob,
            gender:gender
        }

        //
        
        //actual api to update user
        try {
            let result= await axios({
                url: `http://localhost:8000/weeb-users/${id}`,
                method: `PATCH`,
                data: data,
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })

            navigate(`/admin/${id}`)


        } catch (error) {
            toast.error(error.response.data.message)
        }
    }

    let genders = [
        {label:"Male", value:"male"},
        {label:"Female", value:"female"},
        {label:"Other", value:"other"},
    
      ]
      let optionsGender= genders.map((item, i)=>{

          return  (
              <option value= {item.value} key={i}> {item.label}</option>)
  
  
        })

        //data populate to show previous data
        let getAdminUser=async()=>{

            try {
                let result= await axios({
                    url:`http://localhost:8000/weeb-users/${id}`,
                    method:"GET",
                    headers:{
                        Authorization:`Bearer ${token}`
                    }
            })
            console.log(result)
            let data= result.data.data

            setDob(data.dob)
            setfullName(data.fullName)
            setGender(data.gender)
            
            } catch (error) {
                
            }
        }
    
    
        useEffect(()=>{
    
            getAdminUser()
    
        }, [])
  

  return (
    <div>
        <ToastContainer></ToastContainer>
  <form onSubmit={onSubmit}>
            <div><label htmlFor="fullName"> Full Name: </label>
            <input type="text" placeholder="e.g anup" id='fullName'
            value={fullName}
            onChange={(e)=>{
                // console.log("change")
                setfullName(e.target.value)

            }} ></input>
            </div>

          

            <div><label htmlFor="dob"> Date of Birth: </label>
            <input type="date" placeholder="e.g 1996-10-10" id='dob'
            value={dob}
            onChange={(e)=>{
                // console.log("change")
                setDob(e.target.value)

            }} ></input>
            </div>

             <div> <label>Select Gender:</label>
            <select value={gender} onChange={(e)=>{
                setGender(e.target.value)
            }}>
            
            

         {optionsGender}
             
           </select> 
        </div> 

           



            <button type="submit">Update</button>
        </form>


    </div>
  )
}

export default AdminUpdateSpecificUser