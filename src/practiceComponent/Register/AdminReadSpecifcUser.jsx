import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const AdminReadSpecificUser = () => {
    let [user, setUser]=useState({})
    let token=localStorage.getItem("token")
    let navigate =useNavigate()
    let params=useParams()

    let id=params.id

    
    let getAdminUser=async()=>{

        try {
            let result= await axios({
                url:`http://localhost:8000/weeb-users/${id}`,
                method:"GET",
                headers:{
                    Authorization:`Bearer ${token}`
                }
        })
        setUser(result.data.data)
        } catch (error) {
            
        }
    }


    useEffect(()=>{

        getAdminUser()

    })
  return (
    <div>
        <div>
    <p>Full Name :{user.fullName}</p>
     <p>Gender :{user.gender}</p>
       <p>Date :{new Date(user.dob).toLocaleDateString()}</p>
        <p>Email :{user.email}</p>
        <p>Role :{user.role}</p>

        <button onClick={()=>{
            navigate("/admin/user-update")
        }}>Update User</button>
        
        </div>
        
    </div>
  )
}

export default AdminReadSpecificUser