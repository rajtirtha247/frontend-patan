import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';


const AdminPasswordUpdate = () => {
    let token=localStorage.getItem("token")

    let navigate= useNavigate()
    
    let [oldPassword, setOldPassword]=useState("")
    let [newPassword, setNewPassword]=useState("")
  

    
    
    let onSubmit=async (e)=>{

        e.preventDefault()
        console.log("the form is submitted")

        let data={
            oldPassword:oldPassword,
            newPassword:newPassword
            
        }

        //
        
        //actual api to update profile
        try {
            let result= await axios({
                url: `http://localhost:8000/weeb-users/update-password`,
                method: `PATCH`,
                data: data,
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })

            localStorage.removeItem("token")

            navigate("/admin/login")


        } catch (error) {
            toast.error(error.response.data.message)
        }
    }

   

      
  

  return (
    <div>
        <ToastContainer></ToastContainer>
  <form onSubmit={onSubmit}>
            <div><label htmlFor="oldPassword"> old Password  </label>
            <input type="password" placeholder="e.g anup" id='oldPassword'
            value={oldPassword}
            onChange={(e)=>{
                // console.log("change")
                setOldPassword(e.target.value)

            }} ></input>
            </div>

          

            <div><label htmlFor="newPassword"> New Password </label>
            <input type="password" placeholder="e.g 1996-10-10" id='newPassword'
            value={newPassword}
            onChange={(e)=>{
                // console.log("change")
                setNewPassword(e.target.value)

            }} ></input>
            </div>

            
           



            <button type="submit">Update</button>
        </form>


    </div>
  )
}

export default AdminPasswordUpdate