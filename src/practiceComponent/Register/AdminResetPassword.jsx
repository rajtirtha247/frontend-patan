import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';


const AdminResetPassword = () => {


    let [query]=useSearchParams()
    let token=query.get("token")

    let navigate= useNavigate()
    
    let [newPassword, setNewPassword]=useState("")
  

    let onSubmit=async (e)=>{

        e.preventDefault()
        console.log("the form is submitted")

        let data={
            newPassword:newPassword
            
        }

        //
        
        //actual api to update profile
        try {
            let result= await axios({
                url: `http://localhost:8000/weeb-users/reset-password`,
                method: `PATCH`,
                data: data,
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })

            localStorage.removeItem("token")

            navigate("/admin")


        } catch (error) {
            toast.error(error.response.data.message)
        }
    }


  return (
    <div>
        <ToastContainer></ToastContainer>
  <form onSubmit={onSubmit}>
           


            <div><label htmlFor="newPassword"> New Password: </label>
            <input type="password" placeholder="e.g 1996-10-10" id='newPassword'
            value={newPassword}
            onChange={(e)=>{
                // console.log("change")
                setNewPassword(e.target.value)

            }} ></input>
            </div>


            <button style={{cursor:"pointer"}}type="submit">Reset</button>
        </form>


    </div>
  )
}

export default AdminResetPassword