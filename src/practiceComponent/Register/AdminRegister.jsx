import axios from 'axios'
import React, { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';


const AdminRegister = () => {
    let [fullName, setfullName]=useState("")
    let [email, setEmail]=useState("")
    let [password, setPassword]=useState("")
    let [dob, setDob]=useState("")
    let [gender, setGender]=useState("male")

    
    
    let onSubmit=async (e)=>{

        e.preventDefault()
        console.log("the form is submitted")

        let data={
            fullName:fullName,
            email:email,
            password:password,
            dob:dob,
            gender:gender
        }

        data={
            ...data,
            role:"admin"
        }
        
        try {
            let result= await axios({
                url: `http://localhost:8000/weeb-users`,
                method: `POST`,
                data: data
            })

            toast.success("A link has been sent to your email. Please click the given link to verify your account!!")


            setfullName("")
            setEmail("")
            setPassword("")
            setDob("")
            setGender("male")

        } catch (error) {
            toast.error(error.response.data.message)
        }
    }

    let genders = [
        {label:"Male", value:"male"},
        {label:"Female", value:"female"},
        {label:"Other", value:"other"},
    
      ]
      let optionsGender= genders.map((item, i)=>{

          return  (
              <option value= {item.value} key={i}> {item.label}</option>)
  
  
        })
  

  return (
    <div>
        <ToastContainer></ToastContainer>
  <form onSubmit={onSubmit}>
            <div><label htmlFor="fullName"> Full Name: </label>
            <input type="text" placeholder="e.g anup" id='fullName'
            value={fullName}
            onChange={(e)=>{
                // console.log("change")
                setfullName(e.target.value)

            }} ></input>
            </div>

            <div><label htmlFor="email"> Email: </label>
            <input type="email" placeholder="e.g someone@gmail.com" id='email'
            value={email}
            onChange={(e)=>{
                // console.log("change")
                setEmail(e.target.value)

            }} ></input>
            </div>
            

            <div><label htmlFor="password"> Password: </label>
            <input type="password" placeholder="e.g some@123" id='password'
            value={password}
            onChange={(e)=>{
                // console.log("change")
                setPassword(e.target.value)

            }} ></input>
            </div>

            <div><label htmlFor="dob"> Date of Birth: </label>
            <input type="date" placeholder="e.g 1996-10-10" id='dob'
            value={dob}
            onChange={(e)=>{
                // console.log("change")
                setDob(e.target.value)

            }} ></input>
            </div>

             <div> <label>Select Gender:</label>
            <select value={gender} onChange={(e)=>{
                setGender(e.target.value)
            }}>
            
            

         {optionsGender}
             
           </select> 
        </div> 

           



            <button type="submit">Proceed</button>
        </form>


    </div>
  )
}

export default AdminRegister