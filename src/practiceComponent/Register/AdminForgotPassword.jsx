import axios from 'axios';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const AdminForgotPassword = () => {

    let navigate= useNavigate()
    
    let [email, setEmail]=useState("")
  

    
    
    let onSubmit=async (e)=>{

        e.preventDefault()
        console.log("the form is submitted")

        let data={
            email:email,
            
            
        }

        //
        
        //actual api to update profile
        try {
            let result= await axios({
                url: `http://localhost:8000/weeb-users/forgot-password`,
                method: `POST`,
                data: data,
                
            })

            setEmail("")

            toast.success("A link has been sent to your email to reset your password")

            // localStorage.removeItem("token")
            // navigate("/admin/login")


        } catch (error) {
            toast.error(error.response.data.message)
        }
    }

   

      
  

  return (
    <div>
        <ToastContainer></ToastContainer>
  <form onSubmit={onSubmit}>
            <div><label htmlFor="email"> Email  </label>
            <input type="email" placeholder="e.g anup" id='email'
            value={email}
            onChange={(e)=>{
                // console.log("change")
                setEmail(e.target.value)

            }} ></input>
            </div>

            <button type="submit">Forgot Password</button>
        </form>


    </div>
  )
}

export default AdminForgotPassword