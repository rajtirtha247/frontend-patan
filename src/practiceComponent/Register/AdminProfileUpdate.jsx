import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';


const AdminProfileUpdate = () => {
    let token=localStorage.getItem("token")

    let navigate= useNavigate()
    let [fullName, setfullName]=useState("")
    
    let [dob, setDob]=useState("")
    let [gender, setGender]=useState("male")

    
    
    let onSubmit=async (e)=>{

        e.preventDefault()
        console.log("the form is submitted")

        let data={
            fullName:fullName,
            dob:dob,
            gender:gender
        }

        //
        
        //actual api to update profile
        try {
            let result= await axios({
                url: `http://localhost:8000/weeb-users/update-profile`,
                method: `PATCH`,
                data: data,
                headers:{
                    Authorization:`Bearer ${token}`
                }
            })

            navigate("/admin/my-profile")


        } catch (error) {
            toast.error(error.response.data.message)
        }
    }

    let genders = [
        {label:"Male", value:"male"},
        {label:"Female", value:"female"},
        {label:"Other", value:"other"},
    
      ]
      let optionsGender= genders.map((item, i)=>{

          return  (
              <option value= {item.value} key={i}> {item.label}</option>)
  
  
        })

        //data populate to show previous data
        let getAdminProfile=async()=>{

            try {
                let result= await axios({
                    url:"http://localhost:8000/weeb-users/my-profile",
                    method:"GET",
                    headers:{
                        Authorization:`Bearer ${token}`
                    }
            })
            let data= result.data.data
            setDob(data.dob)
            setfullName(data.fullName)
            setGender(data.gender)
            
            } catch (error) {
                
            }
        }
    
    
        useEffect(()=>{
    
            getAdminProfile()
    
        },[])
  

  return (
    <div>
        <ToastContainer></ToastContainer>
  <form onSubmit={onSubmit}>
            <div><label htmlFor="fullName"> Full Name: </label>
            <input type="text" placeholder="e.g anup" id='fullName'
            value={fullName}
            onChange={(e)=>{
                // console.log("change")
                setfullName(e.target.value)

            }} ></input>
            </div>

          

            <div><label htmlFor="dob"> Date of Birth: </label>
            <input type="date" placeholder="e.g 1996-10-10" id='dob'
            value={dob}
            onChange={(e)=>{
                // console.log("change")
                setDob(e.target.value)

            }} ></input>
            </div>

             <div> <label>Select Gender:</label>
            <select value={gender} onChange={(e)=>{
                setGender(e.target.value)
            }}>
            
            

         {optionsGender}
             
           </select> 
        </div> 

           



            <button type="submit">Update</button>
        </form>


    </div>
  )
}

export default AdminProfileUpdate