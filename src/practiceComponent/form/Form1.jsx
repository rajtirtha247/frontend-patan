import React, { useState } from 'react'

const Form1 = () => {



    let [name, setName]=useState("")
    let [surname, setSurname]=useState("")
    let [email, setEmail]=useState("")
    let [password, setPassword]=useState("")
    let [phone, setPhone]=useState("")
    let [date, setDate]=useState("")
    let [description, setDescription]=useState("")

    
    
    let onSubmit=(e)=>{

        e.preventDefault()
        console.log("the form is submitted")

        let data={
            name:name,
            surname:surname,
            email:email,
            password:password,
            description:description
        }

        console.log(data)

    }



  return (

    <div>
        <form onSubmit={onSubmit}>
            <div><label htmlFor="name"> Name: </label>
            <input type="text" placeholder="e.g anup" id='name'
            value={name}
            onChange={(e)=>{
                // console.log("change")
                setName(e.target.value)

            }} ></input>
            </div>

            <div><label htmlFor="surname"> SurName: </label>
            <input type="text" placeholder="e.g khadka" id='surname'
            value={surname}
            onChange={(e)=>{
                // console.log("change")
                setSurname(e.target.value)

            }} ></input>
            </div>

            
            <div><label htmlFor="email"> Email: </label>
            <input type="email" placeholder="e.g someone@gmail.com" id='email'
            value={email}
            onChange={(e)=>{
                // console.log("change")
                setEmail(e.target.value)

            }} ></input>
            </div>

            

            <div><label htmlFor="password"> Paasword: </label>
            <input type="password" placeholder="e.g kallu@123" id='password'
            value={password}
            onChange={(e)=>{
                // console.log("change")
                setPassword(e.target.value)

            }} ></input>
            </div>

            <div><label htmlFor="phonenumber"> Phone Number: </label>
            <input type="number" placeholder="e.g 9779812345678" id='phonenumber'
            value={phone}
            onChange={(e)=>{
                // console.log("change")
                setPhone(e.target.value)

            }} ></input>
            </div>

            <div><label htmlFor="date"> Date of Birth: </label>
            <input type="date" placeholder="e.g 2058-03-17" id='date'
            value={date}
            onChange={(e)=>{
                // console.log("change")
                setDate(e.target.value)

            }} ></input>
            </div>

            <div>
            <label htmlFor="description"> Description: </label>
            <textarea  placeholder="e.g This is description" id='description'
            value={description}
            onChange={(e)=>{
                // console.log("change")
                setDescription(e.target.value)

            }} ></textarea>

            </div>




            <button type="submit">Proceed</button>
        </form>


    </div>
  )
}

export default Form1