import React, { useEffect, useState } from 'react'

const LearnCleanUpFunction = () => {
    let[count, setCount]=useState(0)

    useEffect(()=>{
        console.log("i am useEffect function")

        return ()=>{
console.log("i am clean-Up function")
        }
    }, [count])

    //clean up function are those function which is return by useEffect.
    //Clean up function doesn't execute in first render.
    //Clean up function executes from second render if useEffect function run.


    /* 
    What happens whe useEffect function get run

    first clean up function will run and then the code above it will get

    When components gets unmounted, nothing gets executed except clean up function.
    */
  return (
    <div>
The count is {count} <br></br>
        <button onClick={()=>{

            setCount(count+1)
        }} > Increment Count 1</button>

    </div>
  )
}

export default LearnCleanUpFunction