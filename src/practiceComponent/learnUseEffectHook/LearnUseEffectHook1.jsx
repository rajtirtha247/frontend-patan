import React, { useEffect, useState } from 'react'

const LearnUseEffectHook1 = () => {

    let [count, setCount]= useState(0)
    let [count2, setCount2]= useState(100)

    //we can use multiple useEffect functions.
    useEffect(()=>{

        console.log("i am use Effect function")
    }, [count, count2]) //this executes on first render and from second render depends on count, count2. if count, count2 values changes then it will render.

    useEffect(()=>{

      console.log("i am use Effect function")
  }, [count]) //this executes on first render and from second render depends on count

  useEffect(()=>{

    console.log("i am use Effect function")
}, []) //this executes for single or first render only

useEffect(()=>{

  console.log("i am use Effect function")
}) //executes for each render

console.log("**********") //this also executes for each render

//these two are same; useEffect with no dependency and console.log

  return (
    <div>Count is {count} <br></br>
    
    <button onClick={()=>{
        setCount(count+1)
    }}>Increment</button>
<br></br>
Count2 is {count2} <br></br>
<button onClick={()=>{
        setCount2(count2+1)
    }}>Increment count 2</button>
    </div>
  )
}

export default LearnUseEffectHook1