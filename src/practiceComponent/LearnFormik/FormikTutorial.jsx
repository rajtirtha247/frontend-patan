import { Form, Formik } from 'formik'
import React from 'react'
import * as yup from "yup"
import FormikInput from './FormikInput'
import FormikRadio from './FormikRadio'
import FormikSelect from './FormikSelect'
import FormikCheckBox from './FormikCheckBox'
import FormikTextArea from './FormikTextArea'


const FormikTutorial = () => {
    let initialValues={
        fullName:"",
        email:"",
        password:"",
        gender:"male",   
        country:"Nepal",
        isMarried:false,
        description:"",
        phoneNumber:0,
        age:0

    }

    let onSubmit=(value, others)=>{
        console.log(value)
    }

    let validationSchema= yup.object({
        fullName:yup.string().required("FirstName field is required.").min(5, "Must be at least 5 character")
        .max(20, "must be at most 15 character")
        .matches(/^[a-zA-Z ]*$/, "Only alphabet and space are allowed"),
        email:yup.string().required("email field is required.")
        .matches(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/, "Email is not valid"),
        password:yup.string().required("password field is required.")
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()\-_=+{};:,<.>]).{8,}$/, "Password should contain a minimum of 8 characters, at least one uppercase letter, one lowercase letter, one number, and one special character. ")
        ,
        country:yup.string().required("country field is required."),
        gender:yup.string().required("gender is required."),
        isMarried:yup.boolean(),
        description:yup.string(),
        phoneNumber:yup.number().required("phoneNumber field is required."),
        age:yup.number().required("age field is required.").min(18, "must be 18 year old",),

    })
    let countryOptions=[
        {
            label:"Select Country",
            value:"",
            disabled:true
         },
         {
            label:"Nepal",
            value:"nep"
         },
         {
            label:"India",
            value:"ind"
         },
         {
            label:"China",
            value:"chn"
         },
         {
            label:"Japan",
            value:"jpn"
         },
         {
            label:"America",
            value:"usa"
         },
            
            
            ]

    let genderOptions=[
        {
            label:"Male",
            value:"male",
         },
         {
            label:"Female",
            value:"female",
         },
         {
            label:"Other",
            value:"other",
         },

        ]
  return (
    <div> 
    <Formik 
    initialValues={initialValues} 
    onSubmit={onSubmit}
    validationSchema={validationSchema}
    >
        {
            (formik)=>{
                return (
                <Form>
                <FormikInput
                name="fullName"
                label="Full Name"
                type="text"
                // onChange={(e)=>{
                //     formik.setFieldValue("fullName", e.target.value)
                // }}
                required={true} 
                ></FormikInput>

                <FormikInput
                name="email"
                label="Email:"
                type="email"
                // onChange={(e)=>{
                //     formik.setFieldValue("email", e.target.value)
                // }}
                required={true} 
                ></FormikInput>

                <FormikInput
                name="password"
                label="Password:"
                type="password"
                // onChange={(e)=>{
                //     formik.setFieldValue("password", e.target.value)
                // }}
                required={true} 
                ></FormikInput>

                <FormikRadio
                name="gender"
                label="Gender"
                // onChange={(e)=>{
                //     formik.setFieldValue("gender", e.target.value)
                // }}
                required={true}
                options={genderOptions}
                
                ></FormikRadio>

                <FormikSelect
                name="country"
                label="Country"
                // onChange={(e)=>{
                //     formik.setFieldValue("country", e.target.value)
                // }}
                required={true}
                options={countryOptions}
                
                ></FormikSelect>

                <FormikCheckBox
                name="isMarried"
                label="Is Married?"
                // onChange={(e)=>{
                //     formik.setFieldValue("isMarried", e.target.checked)
                // }}
                
                ></FormikCheckBox>

                <FormikTextArea
                name="description"
                label="Description:"
                // onChange={(e)=>{
                //     formik.setFieldValue("description", e.target.checked)
                // }}
                ></FormikTextArea>

                <FormikInput
                name="phoneNumber"
                label="Phone Number:"
                type="number"
                // onChange={(e)=>{
                //     formik.setFieldValue("phoneNumber", e.target.value)
                // }}
                required={true} 
                ></FormikInput>

                 <FormikInput
                name="age"
                label="Age :"
                type="number"
                // onChange={(e)=>{
                //     formik.setFieldValue("age", e.target.value)
                // }}
                required={true} 
                ></FormikInput>







                   
                <button type="submit">Submit</button>


                </Form>)
            }
        }


    </Formik>



</div>
  )
}

export default FormikTutorial