import { Field } from 'formik'
import React from 'react'

const FormikCheckBox = ({name, label, onChange, required, ...props}) => {
    
  return (
    <div>
        <Field name={name} >
                            {
                                ({field, form, meta})=>{
                                    onChange=!!onChange?onChange:field.onChange

                                    return (<div>
                                        <label htmlFor={name}> {label} {required?<span style={{color:"red"}}>*</span>:null }</label>
                                        <input 
                                        {...field}
                                        {...props}
                                        id={name}
                                        type="checkbox"
                                        checked={meta.value}
                                        onChange={onChange}
                                        
                                        ></input>
                                        {
                                            meta.touched&&meta.error?<div style={{color:"red" }}> {meta.error}</div>:null
                                        }
                                    </div>)
                                }
                            }

                        </Field> 




    </div>
  )
}

export default FormikCheckBox