import { Field } from 'formik'
import React from 'react'

const FormikRadio = ({name, label, onChange, required, options,...props}) => {
    
  return (
    <div>
        <Field name={name} >
                            {
                                ({field, form, meta})=>{
                                    onChange=!!onChange?onChange:field.onChange

                                    return (
                                    <div>
                                        <label htmlFor={name}> {label} {required?<span style={{color:"red"}}>*</span>:null }</label>
                                    
                                    {
                                        options.map((item, i)=>{
                                            return (
                                            <div 
                                                key={i} >
                                            <label htmlFor={item.value}>{item.label}</label>
                                            <input
                                            {...field}
                                            {...props}
                                            id={item.value}
                                            type="radio"
                                            //value
                                            value={item.value}
                                            checked={meta.value===item.value}
                                            onChange={onChange}

                                            ></input>

                                            </div>)
                                        
                                        })
                                    }
                                        
                                        {
    meta.touched&&meta.error?<div style={{color:"red" }}> {meta.error}</div>:null} 
                                        
                                        
                                    </div>)
                                }
                            }

                        </Field> 




    </div>
  )
}

export default FormikRadio