import { Field, Form, Formik } from 'formik'
import React from 'react'
import * as yup from "yup"
import FormikInput from './FormikInput'
import FormikTextArea from './FormikTextArea'
import FormikSelect from './FormikSelect'
import FormikRadio from './FormikRadio'
import FormikCheckBox from './FormikCheckBox'

const FormikForm = () => {

    //each meta has three thing:; value, error, touched
    let initialValues={
        firstName:"",
        lastName:"",
        description:"",
        country:"",

    }

    let onSubmit=(value, others)=>{
        console.log(value)
    }

    let validationSchema= yup.object({
        firstName:yup.string().required("FirstName field is required."),
        lastName:yup.string().required("lastName field is required."),
        description:yup.string().required("description field is required.")
    })

    let countryOptions=[
        {
            label:"Select Country",
            value:"",
            disabled:true
         },
         {
            label:"Nepal",
            value:"nep"
         },
         {
            label:"India",
            value:"ind"
         },
         {
            label:"China",
            value:"chn"
         },
         {
            label:"Japan",
            value:"jpn"
         },
         {
            label:"America",
            value:"usa"
         },
            
            
            ]

    let genderOptions=[
        {
            label:"Male",
            value:"male",
         },
         {
            label:"Female",
            value:"female",
         },
         {
            label:"Other",
            value:"other",
         },

        ]
//validation will run only if
//onChange event is fire,
//onBlur (touched) event is fire,
//onSubmit event is fire
  return (
    <div> 
        <Formik 
        initialValues={initialValues} 
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        
        >
            {
                (formik)=>{
                    return (
                    <Form>
                        <FormikInput 
                        name="firstName" 
                        label="First Name"
                        type="text"
                        onChange={(e)=>{
                                formik.setFieldValue("firstName", e.target.value)
                            }}
                        placeholder="FirstName"
                        required={true}
                        
                        ></FormikInput>

                        <FormikInput 
                        name="lastName" 
                        label="Last Name"
                        type="text"
                        onChange={(e)=>{
                            formik.setFieldValue("firstName", e.target.value)
                        }}
                        placeholder="LastName"
                        required={true}
                        >
                            
                        </FormikInput>\

                        <FormikTextArea 
                        name="description" 
                        label="Description"
                        type="text"
                        onChange={(e)=>{
                            formik.setFieldValue("firstName", e.target.value)
                        }}
                        placeholder="LastName"
                        required={true}
                        >
                            
                        </FormikTextArea>

                        <FormikSelect
                        name="country" 
                        label="Country"
                        onChange={(e)=>{
                            formik.setFieldValue("country", e.target.value)
                        }}
                        required={true}
                        options={countryOptions}
                        >
                            
                        </FormikSelect>

                        <FormikRadio
                         name="gender" 
                         label="Gender"
                         onChange={(e)=>{
                             formik.setFieldValue("gender", e.target.value)
                         }}
                         required={true}
                         options={genderOptions}
                        
                        
                        ></FormikRadio>

                        <FormikCheckBox
                        name="isMarried" 
                        label="Is Married"
                        onChange={(e)=>{
                            formik.setFieldValue("isMarried", e.target.checked)
                        }}
                        >

                        </FormikCheckBox>


                        
                        
                        {/* <Field name="firstName">
                            {
                                ({field, form, meta})=>{
                                    return (<div>
                                        <label htmlFor="firstName">First Name</label>
                                        <input 
                                        {...field}
                                        id="firstName" 
                                        type="text" value={meta.value} 
                                        onChange={field.onChange}
                                        // onChange={(e)=>{
                                        //     formik.setFieldValue("firstName", e.target.value)
                                        // }}
                                        ></input>
                                        {
                                            meta.touched&&meta.error?<div style={{color:"red" }}> {meta.error}</div>:null
                                        }
                                    </div>)
                                }
                            }

                        </Field> 

                        <Field name="lastName">
                            {
                                ({field, form, meta})=>{
                                    return (<div>
                                        <label htmlFor="lastName">Last Name</label>
                                        <input
                                        {...field}
                                        id="lastName" 
                                        type="text" 
                                        value={meta.value} 
                                        onChange={field.onChange}
                                        // onChange={(e)=>{
                                        //     formik.setFieldValue("lastName", e.target.value)
                                        // }}
                                        ></input>
                                        {
                                           meta.touched&&meta.error?<div style={{color:"red" }}> {meta.error}</div>:null
                                        }
                                    </div>)
                                }
                            }

                        </Field>
 
                        <Field name="description">
                            {
                                ({field, form, meta})=>{
                                    return (<div>
                                        <label htmlFor="lastName">Description</label>
                                        <input
                                        {...field} 
                                        id="lastName" 
                                        type="text" 
                                        value={meta.value} 
                                        onChange={field.onChange}
                                        // onChange={(e)=>{
                                        //     formik.setFieldValue("description", e.target.value)
                                        // }}
                                        ></input>
                                        {
                                            meta.touched&&meta.error?<div style={{color:"red" }}> {meta.error}</div>:null
                                        }
                                    </div>)
                                }
                            }

                        </Field> */}
                    
                    <button type="submit">Submit</button>


                    </Form>)
                }
            }


        </Formik>



    </div>
  )
}

export default FormikForm