import React from 'react'
import { Route, Routes } from 'react-router-dom'
import CreateProduct from './product/CreateProduct'
import ReadAllStudents from './student/ReadAllStudents'
import CreateStudent from './student/CreateStudent'
import ReadAllProducts from './product/ReadAllProducts'
import ReadSpecificProduct from './product/ReadSpecificProduct'
import ReadSpecificStudent from './student/ReadSpecificStudent'

const MyRoutes = () => {


  return (
    <div>
        <Routes>
        <Route path="/" element={<div>This is Homepage</div>}></Route>
            
            <Route path="/products/create" element={<CreateProduct></CreateProduct>}></Route>

            <Route path="/products" element={<ReadAllProducts></ReadAllProducts>}></Route>

            <Route path="/products/:id" element={<ReadSpecificProduct></ReadSpecificProduct>}></Route>

            <Route path="/students/createStudents" element={<CreateStudent></CreateStudent>}></Route>

            <Route path="/students" element={<ReadAllStudents></ReadAllStudents>}></Route>
            
            <Route path="/students/:id" element={<ReadSpecificStudent></ReadSpecificStudent>}></Route>

            <Route path="/*" element={<div>Error 404 Page Not Found</div>}></Route>

        </Routes>



    </div>
  )
}

export default MyRoutes