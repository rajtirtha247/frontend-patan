import React from 'react'

const RemoveLocalStorageData = () => {
  return (
    <div >

<button onClick={()=>{
    localStorage.removeItem("token")
}} style={{margin:"20px"}}>Remove Token </button> <br></br>

<button onClick={()=>{
    localStorage.removeItem("name")
}} style={{margin:"20px"}}>Remove Name </button> <br></br>


<button onClick={()=>{
    localStorage.removeItem("age")
}} style={{margin:"20px"}}>Remove Age </button> <br></br>


<button onClick={()=>{
    localStorage.removeItem("isMarried")
}} style={{margin:"20px"}}>Remove isMarried </button>

    </div>
    
  )
}

export default RemoveLocalStorageData