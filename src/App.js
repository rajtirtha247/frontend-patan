import { useState } from 'react';
import './App.css';
import Info from './css';
import Address from './practiceComponent/Address';
import Age from './practiceComponent/Age';
import ButtonClick from './practiceComponent/ButtonClicl';
import College from './practiceComponent/College';
import Detail from './practiceComponent/Detail';
import Detail1 from './practiceComponent/Detail1';
import EffectOnDifferentData from './practiceComponent/EffectOnDifferentData';
import LearnMap1 from './practiceComponent/LearnMap1';
import LearnMap2 from './practiceComponent/LearnMap2';
import Name from './practiceComponent/Name';
import LearnTearnary from './practiceComponent/learnTearnary';
import LearnCleanUpFunction from './practiceComponent/learnUseEffectHook/LearnCleanUpFunction';
import LearnUseEffectHook1 from './practiceComponent/learnUseEffectHook/LearnUseEffectHook1';
import Count from './practiceComponent/learnUseStateHook/Count';
import IncrementByChoice from './practiceComponent/learnUseStateHook/IncrementByChoice';
import Namee from './practiceComponent/learnUseStateHook/Namee';
import ShowAndHideImage from './practiceComponent/learnUseStateHook/ShowAndHideImage';
import ShowImg from './practiceComponent/learnUseStateHook/ShowImg';
import Toggle from './practiceComponent/learnUseStateHook/Toggle';
import LearnUseStateHook2 from './practiceComponent/learnUseStateHook/learnUseState2';
import LearnUseStateHook1 from './practiceComponent/learnUseStateHook/learnUseStateHook1';
import WhyUseState from './practiceComponent/learnUseStateHook/whyUseState';
import MyLinks from './practiceComponent/MyLinks';
import MyRoutes from './practiceComponent/MyRoutes';
import Form1 from './practiceComponent/form/Form1';
import Form2 from './practiceComponent/form/Form2';
import NestingRoute from './practiceComponent/Route/NestingRoute';
import ProductNesting from './practiceComponent/Route/NestingTask';
import Form3 from './practiceComponent/form/Form3';
import Learn1UseRefHook from './practiceComponent/learnUseRefHook/learn1UseRefHook';
import AddDataToLocalStorage from './practiceComponent/LearnLocalStorage/AddDataToLocalStorage';
import GetLocalStorageData from './practiceComponent/LearnLocalStorage/GetLocalStorageData';
import RemoveLocalStorageData from './practiceComponent/LearnLocalStorage/RemoveLocalStorageData';
import AddDataToSessionStorage from './practiceComponent/LearnSessionStorage/AddDataToSessionStorage';
import GetDataFromSessionStorage from './practiceComponent/LearnSessionStorage/GetDataFromSessionStorage';
import FormikForm from './practiceComponent/LearnFormik/FormikForm';
import FormikTutorial from './practiceComponent/LearnFormik/FormikTutorial';

function App() {
  // let a= <p style={{color:"gold", backgroundColor:"red"}}> i am a paragraph.</p>
  // let name="Anup Khadka"
  // let age= 22

  let [shouldShowComponent, setIsHide]=useState(false)

  let handleComponent=(isShown)=>{

    return (e)=>{
        setIsHide(isShown) 
    }

}
  return (

   <div>
    {/* {a}
    {name} <br/>
    {age} <br></br>

  
    <h1 style={{color:"Gold", backgroundColor:"Blue"}}>This Heading.</h1>
    <span className="success">This is span.</span>
    <a href="https://facebook.com" target="">Facebook</a> <br></br>
    <img src="./logo192.png"></img>
    <img src="./images/abc.jpg"></img> */}

    {/* <Name></Name>
    <Age></Age>
    <Address></Address>
    <Detail name="anup" address="itahari" age={22} ></Detail> */}

    {/* <Detail1 name="anup" address="itahari" age={22}></Detail1>

    <College name="Patan Multiple Campus" location="Patandhoka"></College> */}

{/* <LearnTearnary></LearnTearnary> 
<LearnMap1></LearnMap1>

<LearnMap2></LearnMap2> */}

{/* <ButtonClick></ButtonClick> */}

{/* <LearnUseStateHook1></LearnUseStateHook1>

<LearnUseStateHook2></LearnUseStateHook2> */}

{/* <ShowAndHideImage></ShowAndHideImage> */}

{/* <ShowImg></ShowImg> */}

{/* <Namee></Namee>

<IncrementByChoice></IncrementByChoice> */}

{/* <Toggle></Toggle> */}
{/* <Info></Info> */}

{/* <WhyUseState></WhyUseState> */}

{/* <Count></Count> */}

{/* <LearnUseEffectHook1></LearnUseEffectHook1> */}

{/* <LearnCleanUpFunction></LearnCleanUpFunction> */}

{/* { shouldShowComponent?  <div> {LearnCleanUpFunction}</div>:null} */}

{/* {shouldShowComponent?  <LearnCleanUpFunction></LearnCleanUpFunction>:null}
<button onClick={handleComponent(true)}>Show</button>
<button onClick={handleComponent(false)}>Hide</button> */}


{/* {shouldShowComponent && <LearnCleanUpFunction/>} */}


{/* <EffectOnDifferentData></EffectOnDifferentData> */}

{/* <MyLinks></MyLinks>

<MyRoutes></MyRoutes> */}

{/* <Form1></Form1> */}

{/* <Form2></Form2> */}

{/* <NestingRoute></NestingRoute> */}

{/* <ProductNesting></ProductNesting> */}

{/* <Form3></Form3> */}

{/* <Learn1UseRefHook></Learn1UseRefHook> */}

{/* <AddDataToLocalStorage></AddDataToLocalStorage> */}

{/* <GetLocalStorageData></GetLocalStorageData> */}

{/* <RemoveLocalStorageData></RemoveLocalStorageData> */}

{/* <AddDataToSessionStorage></AddDataToSessionStorage> */}

{/* <GetDataFromSessionStorage></GetDataFromSessionStorage> */}

{/* <FormikForm></FormikForm> */}

<FormikTutorial></FormikTutorial>
   </div>
  )
}

export default App;


















